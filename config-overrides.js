// react中集成的第3对于本身webpack的追加配置，修改此文件，一定要重启服务
const path = require('path')
const {
  addDecoratorsLegacy,
  override,
  addWebpackAlias,
  fixBabelImports
} = require('customize-cra')

// 自定义
/* const customize = () => (config) => {
  config.resolve.alias['@'] = path.join(__dirname, 'src')
  return config
} */

module.exports = override(
  addDecoratorsLegacy(),
  // customize()
  addWebpackAlias({
    '@': path.join(__dirname, 'src')
  }),
  // 按需加载antd-mobile组件库所用，antd-mobile只针对于移动站点
  fixBabelImports('import', {
    libraryName: 'antd-mobile',
    style: 'css',
  })
)


